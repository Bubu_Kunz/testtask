package com.example.yan.imagesender.Model;

import android.content.ContentValues;
import android.database.Cursor;
import com.example.yan.imagesender.DB.DBHelper;

public class Message {
    private long m_nId = -1;
    private String m_strEmail = "";
    private String m_strSubject = "";
    private String m_strBody = "";
    private long m_nIdInfo = -1;
    private String m_strImagePath = "";

    @Override
    public int hashCode() {
        return (int) (m_nId ^ (m_nId >>> 32));
    }

    public Message(long nId, long nIdInfo, String strEmail, String strSubject, String strBody,
                   String strImagePath) {
        m_nId = nId;
        m_nIdInfo = nIdInfo;
        m_strEmail = strEmail;
        m_strSubject = strSubject;
        m_strBody = strBody;
        m_strImagePath = strImagePath;
    }

    public Message(Cursor cursor){
        setId(cursor.getLong(0));
        setIdInfo(cursor.getLong(1));
        setEmail(cursor.getString(2));
        setSubject(cursor.getString(3));
        setBody(cursor.getString(4));
        setImagePath(cursor.getString(5));

    }
    public Message(long nId, Cursor cursor){
        setId(nId);
        setIdInfo(cursor.getLong(0));
        setEmail(cursor.getString(1));
        setSubject(cursor.getString(2));
        setBody(cursor.getString(3));
        setImagePath(cursor.getString(4));
    }
    public Message(){

    }

    public long getIdInfo(){
        return m_nIdInfo;
    }

    public void setIdInfo(long idInfo){
        m_nIdInfo = idInfo;
    }

    public void setImagePath(String strImagePath) {
        this.m_strImagePath = strImagePath;
    }

    public String getBody(){
        return  m_strBody;
    }

    public void setBody(String text){
        m_strBody = text;
    }

    public String getSubject(){
        return m_strSubject;
    }

    public void setSubject(String subject){
        m_strSubject = subject;
    }
    public String getImagePath() {

        return m_strImagePath;
    }

    public long getId() {
        return m_nId;
    }

    public String getEmail() {
        return m_strEmail;
    }

    public void setId(long Id) {
        this.m_nId = Id;
    }

    public void setEmail(String Email) {
        this.m_strEmail = Email;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.MESSAGES_ID_INFO, getIdInfo());
        contentValues.put(DBHelper.MESSAGES_EMAIL, getEmail());
        contentValues.put(DBHelper.MESSAGES_SUBJECTS, getSubject());
        contentValues.put(DBHelper.MESSAGES_BODY, getBody());
        contentValues.put(DBHelper.MESSAGES_IMAGE_PATH, getImagePath());
        return contentValues;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        Message message = (Message) obj;
        if (m_nId != message.m_nId) return false;
        if (m_nIdInfo != message.m_nIdInfo) return false;
        if (!m_strEmail.equals(message.m_strEmail)) return false;
        if (!m_strSubject.equals(message.m_strSubject)) return false;
        if (!m_strBody.equals(message.m_strBody)) return false;
        if (!m_strImagePath.equals(message.m_strImagePath)) return false;
        return true;
    }
}
