package com.example.yan.imagesender.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.yan.imagesender.Model.Message;
import com.example.yan.imagesender.R;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MessageAdapter extends BaseAdapter {
    ArrayList<Message> m_arrData = null;
    Context m_Context= null;
    LayoutInflater m_Inflator = null;
    Message m_Message = null;
    public MessageAdapter(Context context, ArrayList<Message> arrData) {
        m_Context = context;
        m_arrData = arrData;
        m_Inflator = (LayoutInflater) m_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Message getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view==null){
            view = m_Inflator.inflate(R.layout.item_message, parent, false);
        }
        TextView tvAdress = (TextView) view.findViewById(R.id.email_adressTV);
        TextView tvSubject = (TextView) view.findViewById(R.id.titleTV);
        ImageView ivImage = (ImageView) view.findViewById(R.id.item_imageIV);
        m_Message = getItem(position);
        tvAdress.setText(m_Message.getEmail());
        tvSubject.setText(m_Message.getSubject());
        putImageToImageView(ivImage);
        return view;
    }
    private void putImageToImageView(ImageView imageView){
        if(m_Message !=null && m_Message.getImagePath().equals("")){
            imageView.setImageResource(R.drawable.epmty_item_image);
        }
        else {
            Bitmap img = loadImageFromInternal(m_Context, m_Message.getImagePath());
            imageView.setImageBitmap(img);
        }
    }

    public static Bitmap loadImageFromInternal(Context context, String picName){
        Bitmap bitmap = null;
        FileInputStream fis;
        try {
            fis = context.openFileInput(picName);
            bitmap = BitmapFactory.decodeStream(fis);
            fis.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


}
