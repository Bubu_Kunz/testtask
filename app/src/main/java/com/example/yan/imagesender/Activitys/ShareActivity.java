package com.example.yan.imagesender.Activitys;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yan.imagesender.Dialogs.DialogAddImage;
import com.example.yan.imagesender.Model.Message;
import com.example.yan.imagesender.Model.Wrappers.DBWrappers.MessageDBWrapper;
import com.example.yan.imagesender.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ShareActivity extends Activity implements View.OnClickListener{
    private static final int REQUEST_SEND_MESSAGE = 144;
    private static final int REQUEST_PERMISSION_READ_EXTERNAL = 101;
    public static final int REQUEST_PERMISSION_CAMERA = 102;
    public static final int REQUEST_CAMERA = 12;

    private static boolean m_sBMailClientIsOpen = false;

    private DialogAddImage m_DialogAddImage = null;
    private TextView m_AddImageTextView = null;
    private ImageView m_AddImageImageView = null;
    private Bitmap m_BitmapImage = null;
    private EditText m_EmailEditText = null, m_SubjectEditText = null, m_BodyEditText = null;
    private Button m_ShareButton = null;
    private Message m_StartMessage = null;
    private Message m_Message = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_share);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_share_screen);
        Intent intent = getIntent();
        long nId = intent.getLongExtra(MainActivity.EXTRA_KEY_ID,-1);
        m_AddImageImageView = (ImageView) findViewById(R.id.addImageView_AcivityShare);
        m_EmailEditText = (EditText) findViewById(R.id.emailEditText_ActivityShare);
        m_SubjectEditText = (EditText) findViewById(R.id.subjectEditText_ActivityShare);
        m_BodyEditText = (EditText) findViewById(R.id.bodyEditText_ActivityShare);
        m_ShareButton = (Button) findViewById(R.id.shareButton_ActivityShare);
        m_AddImageTextView = (TextView) findViewById(R.id.tvAddImage);
        if(nId < 0){
            m_Message = new Message();
        } else {
            MessageDBWrapper dbWrapper = new MessageDBWrapper(this);
            m_Message = dbWrapper.getItemById(nId);
            m_EmailEditText.setText(m_Message.getEmail());
            m_SubjectEditText.setText(m_Message.getSubject());
            m_BodyEditText.setText(m_Message.getBody());

        }
        if(m_Message !=null){
            m_StartMessage = new Message(m_Message.getId(), m_Message.getIdInfo(), m_Message.getEmail(),
                    m_Message.getSubject(), m_Message.getBody(), m_Message.getImagePath());
        }

        putImageToImageView(m_AddImageImageView);
        m_ShareButton.setOnClickListener(this);
        m_AddImageImageView.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        m_sBMailClientIsOpen = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        m_sBMailClientIsOpen = true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.shareButton_ActivityShare:
                putMessageIntoDB(m_Message);
                    if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED){
                        sendEmail();
                    }  else {
                        ActivityCompat.requestPermissions(this, new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        },
                                REQUEST_PERMISSION_READ_EXTERNAL);
                }

                break;
            case R.id.addImageView_AcivityShare:
                FragmentManager fm = getFragmentManager();
                m_DialogAddImage = new DialogAddImage();
                m_DialogAddImage.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                m_DialogAddImage.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
                m_DialogAddImage.show(fm, "");
                break;
            default:
                break;
        }
    }

    public void sendEmail(){
        String[] TO = {m_Message.getEmail()};
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, m_Message.getSubject());
        emailIntent.putExtra(Intent.EXTRA_TEXT, m_Message.getBody());
        File image = null;
        if(!m_Message.getImagePath().equals("")) {
            String strOutputPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/tmpSend.jpg";
            m_BitmapImage = loadImageFromInternal(this, m_Message.getImagePath());
            copyImageToExternal(strOutputPath, m_BitmapImage);
            image = new File(strOutputPath);
            Uri imageUri = Uri.fromFile(image);
            emailIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        }
        try {
            this.startActivityForResult(Intent.createChooser(emailIntent, "Send mail..."), REQUEST_SEND_MESSAGE);
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ShareActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }

    }

    public void doCameraBtnClick(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED){
            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            this.startActivityForResult(intentCamera, REQUEST_CAMERA);

        }else {

                    ActivityCompat.requestPermissions(this, new String[]{
                                    Manifest.permission.CAMERA
                    },
                            REQUEST_PERMISSION_CAMERA);
            Toast toast = Toast.makeText(null, R.string.toast_text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        putMessageIntoDB(m_Message);
        Intent returnIntent = new Intent(this, MainActivity.class);
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DialogAddImage.REQUEST_GALLERY && resultCode == RESULT_OK){
            final Uri selectedImageUri = data.getData();
                try {
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    Rect rect = new Rect(0, 0, 0, 0);
                    BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri), rect, bmOptions);
                    long size = bmOptions.outWidth*bmOptions.outHeight;
                    bmOptions.inJustDecodeBounds = false;
                    int nScale = (int)size/(1024*1024);
                    bmOptions.inSampleSize = nScale;
                    m_BitmapImage = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri), rect, bmOptions);
                    String strImageName =  generateFileName(ShareActivity.this);
                    saveImageToInternal(m_BitmapImage, strImageName, true);
                    m_Message.setImagePath(strImageName);
                    putImageToImageView(m_AddImageImageView);
                } catch (IOException e) {
                    e.printStackTrace();
                }


        }
        if(requestCode==REQUEST_SEND_MESSAGE && m_sBMailClientIsOpen){
                m_Message = new Message();
                m_StartMessage = new Message();
                m_EmailEditText.setText(m_Message.getEmail());
                m_SubjectEditText.setText(m_Message.getSubject());
                m_BodyEditText.setText(m_Message.getBody());
                putImageToImageView(m_AddImageImageView);
        }
        if(requestCode==REQUEST_CAMERA && resultCode == RESULT_OK){
            m_BitmapImage = (Bitmap) data.getExtras().get("data");
            String strImageName = generateFileName(ShareActivity.this);
            saveImageToInternal(m_BitmapImage, strImageName, false);
            m_Message.setImagePath(strImageName);
            putImageToImageView(m_AddImageImageView);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode== REQUEST_PERMISSION_READ_EXTERNAL){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                sendEmail();
            }
            else {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_text, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        if(requestCode==REQUEST_PERMISSION_CAMERA){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                this.startActivityForResult(intentCamera, REQUEST_CAMERA);
            }
            else {
                Toast toast = Toast.makeText(null, R.string.toast_text, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
    private void putImageToImageView(ImageView imageView){
        if(m_Message !=null && m_Message.getImagePath().equals("")){
            imageView.setImageResource(R.drawable.empty_image);
            m_AddImageTextView.setText(R.string.image_add);
        }
        else {
            Bitmap img = loadImageFromInternal(this, m_Message.getImagePath());
            m_AddImageTextView.setText("");
            imageView.setImageBitmap(img);

        }
    }
    public void putMessageIntoDB(Message message){
        MessageDBWrapper wrapper = new MessageDBWrapper(this);
        message.setEmail(m_EmailEditText.getText().toString());
        message.setSubject(m_SubjectEditText.getText().toString());
        message.setBody(m_BodyEditText.getText().toString());
        if(!message.equals(m_StartMessage)){
            if(message.getId()>=0){
                wrapper.updateItem(message);
            } else {
                wrapper.insertItem(message);
            }
        }
    }
    public void saveImageToInternal(Bitmap bitmap, String picName, boolean isFromGallery){
        FileOutputStream fos;
        try {
            fos = openFileOutput(picName, Context.MODE_PRIVATE);
            if(isFromGallery){
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } else {
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, fos);
            }

            fos.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void copyImageToExternal(String strOutputPath, Bitmap inputImage){
        OutputStream out = null;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED){
            try {
                out = new FileOutputStream(strOutputPath);
                inputImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                out = null;
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_text, Toast.LENGTH_SHORT);
            toast.show();
        }


    }
    public static Bitmap loadImageFromInternal(Context context, String picName){
        Bitmap bitmap = null;
        FileInputStream fis;
        try {
            fis = context.openFileInput(picName);
            bitmap = BitmapFactory.decodeStream(fis);
            fis.close();

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    private String generateFileName(Context context){
        MessageDBWrapper wrapper = new MessageDBWrapper(context);
        String strName = ""+wrapper.getMaxId()+".jpeg";
        return strName;
    }

}





