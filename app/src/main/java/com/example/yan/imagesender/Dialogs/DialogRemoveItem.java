package com.example.yan.imagesender.Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yan.imagesender.Activitys.MainActivity;
import com.example.yan.imagesender.R;

public class DialogRemoveItem extends DialogFragment implements View.OnClickListener {
    public static final String REQUEST_YES = "yes";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_remove_item, null);
        view.findViewById(R.id.btnNo).setOnClickListener(this);
        view.findViewById(R.id.btnYes).setOnClickListener(this);
        setStyle(this.STYLE_NO_TITLE, 0);
        setStyle(this.STYLE_NO_FRAME, 0);
        return view;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnYes:
                Intent intent = new Intent();
                intent.putExtra(REQUEST_YES, true);
                ((MainActivity)getActivity()).doPositiveButtonClick();
                dismiss();
                break;
            case R.id.btnNo:
               dismiss();
                break;
        }

    }
}
