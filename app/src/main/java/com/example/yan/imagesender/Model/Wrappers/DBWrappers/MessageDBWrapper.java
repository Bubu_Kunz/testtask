package com.example.yan.imagesender.Model.Wrappers.DBWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.yan.imagesender.DB.DBHelper;
import com.example.yan.imagesender.Model.Message;
import java.util.ArrayList;

public class MessageDBWrapper extends BaseDBWrapper{
    public MessageDBWrapper(Context context) {
        super(context, DBHelper.TABLE_NAME_MESSAGES);
    }

    public ArrayList<Message> getAllMessages(){
        ArrayList<Message> arrMessages = new ArrayList<>();
        SQLiteDatabase db = getDBRead();
        Cursor cursor = db.query(DBHelper.TABLE_NAME_MESSAGES, null, null, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()){
            do{
                arrMessages.add(new Message(cursor));
            }
            while (cursor.moveToNext());
        }
        if(cursor!=null){
            cursor.close();
        }
        db.close();
        return arrMessages;
    }
    public long insertItem(Message item){
        SQLiteDatabase db = getDBWrite();
        return db.insert(getTableName(), null, item.getContentValues());

    }
    public Message getItemById(long nId){
        Message result = null;
        if (nId>=0){
            SQLiteDatabase db = getDBRead();
            String[] arrColumns = new String[]{DBHelper.MESSAGES_ID_INFO, DBHelper.MESSAGES_EMAIL,
                    DBHelper.MESSAGES_SUBJECTS, DBHelper.MESSAGES_BODY, DBHelper.MESSAGES_IMAGE_PATH};
            String strWhere = DBHelper.MESSAGES_ID + "=?";
            Cursor cursor = db.query(getTableName(),arrColumns ,
                    strWhere,
                    new String[]{Long.toString(nId)}, null, null, null);
            if(cursor !=null && cursor.moveToFirst()){
                result = new Message(nId, cursor);
            }
            if(cursor!=null){
                cursor.close();
            }
        }
        return result;
    }
    public void updateItem(Message item){
        SQLiteDatabase db = getDBWrite();
        db.update(getTableName(), item.getContentValues(), DBHelper.MESSAGES_ID+" = ?",
                new String[]{Long.toString(item.getId())});
        db.close();
    }
    public void removeItem(Message item){
        SQLiteDatabase db = getDBWrite();
        String strRec = DBHelper.MESSAGES_ID + " =? ";
        String[] arrSearch = new String[]{Long.toString(item.getId())};
        db.delete(getTableName(), strRec, arrSearch);
        db.close();
    }
    public void removeAll(){
        SQLiteDatabase db = getDBWrite();
        db.delete(getTableName(), null, null);
        db.close();
    }
    public long getMaxId(){
        SQLiteDatabase db = getDBRead();
        Cursor cursor = db.query(DBHelper.TABLE_NAME_MESSAGES,
                new String[]{"MAX("+DBHelper.MESSAGES_ID+")"},null,  null, null, null, null);
        long id = 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getLong(0);
            } while(cursor.moveToNext());
        }
        if(cursor!=null){
            cursor.close();
        }
        return id;
    }
}
