package com.example.yan.imagesender.Model.Wrappers.DBWrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.example.yan.imagesender.DB.DBHelper;


public abstract class BaseDBWrapper {

    private String m_strTableName = "";
    private Context m_context = null;

    public BaseDBWrapper (Context context, String strTableName){
        m_strTableName = strTableName;
        m_context = context;
    }
    public String getTableName(){
        return m_strTableName;
    }
    public Context getContext (){
        return m_context;
    }
    public SQLiteDatabase getDBWrite(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db;
    }
    public SQLiteDatabase getDBRead(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db;
    }
}
