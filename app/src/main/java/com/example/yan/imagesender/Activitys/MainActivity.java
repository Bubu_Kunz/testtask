package com.example.yan.imagesender.Activitys;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import com.example.yan.imagesender.Adapters.MessageAdapter;
import com.example.yan.imagesender.Dialogs.DialogRemoveItem;
import com.example.yan.imagesender.Model.Message;
import com.example.yan.imagesender.Model.Wrappers.DBWrappers.MessageDBWrapper;
import com.example.yan.imagesender.R;
import java.io.File;
import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {
    public static final String EXTRA_KEY_ID = "EXTRA_KEY_ID";
    public static final String EXTRA_KEY_EMAIL = "EXTRA_KEY_EMAIL";
    public static final String EXTRA_KEY_SUBJECT = "EXTRA_KEY_SUBJECT";
    public static final String EXTRA_KEY_BODY = "EXTRA_KEY_BODY";
    public static final String EXTRA_KEY_PATH = "EXTRA_KEY_PATH";
    private static final int REQUEST_SHARE = 1435;

    private long m_nDeletedItemId = -1;

    private ListView m_MessagesListView = null;
    private Button m_RemoveAllButton = null;
    private MessageAdapter m_Adapter = null;
    private MessageDBWrapper m_Wrapper = null;
    private ArrayList<Message> m_arrMessages = new ArrayList<>();
    protected ImageView m_AddButtonImageView;
    private DialogRemoveItem dialogRemoveItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_main_screen);

        m_Wrapper = new MessageDBWrapper(this);
        m_arrMessages.addAll(m_Wrapper.getAllMessages());
        m_Adapter = new MessageAdapter(this, m_arrMessages);

        m_MessagesListView = (ListView) findViewById(R.id.messagesLV);
        m_RemoveAllButton = (Button) findViewById(R.id.removeAllBtn);
        m_AddButtonImageView = (ImageView) findViewById(R.id.btnAdd);

        m_MessagesListView.setAdapter(m_Adapter);
        m_RemoveAllButton.setOnClickListener(this);
        m_AddButtonImageView.setOnClickListener(this);

        m_MessagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ShareActivity.class);
                Message currMessage = m_Adapter.getItem(i);
                intent.putExtra(EXTRA_KEY_ID, currMessage.getId());
                intent.putExtra(EXTRA_KEY_EMAIL, currMessage.getEmail());
                intent.putExtra(EXTRA_KEY_SUBJECT, currMessage.getSubject());
                intent.putExtra(EXTRA_KEY_BODY, currMessage.getBody());
                intent.putExtra(EXTRA_KEY_PATH, currMessage.getImagePath());
                startActivityForResult(intent, REQUEST_SHARE);
            }
        });
        m_MessagesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                m_nDeletedItemId = m_Adapter.getItemId(i);
                dialogRemoveItem = new DialogRemoveItem();
                FragmentManager fm = getFragmentManager();
                dialogRemoveItem.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                dialogRemoveItem.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
                dialogRemoveItem.show(fm, "");
                return false;
            }
        });

    }

    @Override
    protected void onDestroy() {
        deleteImageCopy();
        super.onDestroy();
    }

    public void doPositiveButtonClick(){
        deleteItem();
        m_nDeletedItemId = -1;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        m_Adapter.notifyDataSetChanged();
        if(requestCode==REQUEST_SHARE){
            if(resultCode==RESULT_CANCELED){
            m_Adapter = new MessageAdapter(MainActivity.this, m_Wrapper.getAllMessages());
            m_MessagesListView.setAdapter(m_Adapter);
            m_Adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.removeAllBtn:
                for(Message m: m_Wrapper.getAllMessages()){
                    if(!m.getImagePath().equals("")){
                        this.deleteFile(m.getImagePath());
                    }
                }
                m_Wrapper.removeAll();
                m_Adapter = new MessageAdapter(MainActivity.this, m_Wrapper.getAllMessages());
                m_MessagesListView.setAdapter(m_Adapter);
                break;
            case R.id.btnAdd:
                Intent intent = new Intent(MainActivity.this, ShareActivity.class);
                intent.putExtra(EXTRA_KEY_ID, String.valueOf(-1));
                startActivityForResult(intent, REQUEST_SHARE);
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        m_Adapter.notifyDataSetChanged();

    }

    public void deleteItem(){
        if(m_nDeletedItemId >=0) {
            String strPath = m_Wrapper.getItemById(m_nDeletedItemId).getImagePath();
            if (!strPath.equals("")) {
                deleteFile(strPath);
            }
            Message currMessage = m_Wrapper.getItemById(m_nDeletedItemId);
            m_Wrapper.removeItem(currMessage);
            m_Adapter = new MessageAdapter(MainActivity.this, m_Wrapper.getAllMessages());
            m_MessagesListView.setAdapter(m_Adapter);
        }
    }
     private void deleteImageCopy(){
         String strFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                 .getAbsolutePath()+"/tmpSend.jpg";
         if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                 == PackageManager.PERMISSION_GRANTED){
             File file = new File(strFilePath);
             if(file.exists()){
                 boolean deleted = file.delete();
             }

         }
     }

}
