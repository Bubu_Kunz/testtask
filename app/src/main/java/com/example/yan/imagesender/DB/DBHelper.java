package com.example.yan.imagesender.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_NAME_MESSAGES = "DRAFT_AND_SENT_MESSAGES";
    public static final String MESSAGES_ID = "_ID";
    public static final String MESSAGES_ID_INFO = "ID_INFO";
    public static final String MESSAGES_EMAIL = "EMAIL";
    public static final String MESSAGES_SUBJECTS = "SUBJECTS";
    public static final String MESSAGES_BODY = "BODY";
    public static final String MESSAGES_IMAGE_PATH = "IMAGE_PATH";

    public DBHelper(Context context){
        super(context, "messages.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME_MESSAGES + "("
            + MESSAGES_ID + "  integer primary key autoincrement, "
            + MESSAGES_ID_INFO + " integer, "
            + MESSAGES_EMAIL + " text, "
            + MESSAGES_SUBJECTS + " text, "
            + MESSAGES_BODY + " text, "
            +MESSAGES_IMAGE_PATH + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int nOldVersion, int nNewVersion) {

    }
}
