package com.example.yan.imagesender.Dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yan.imagesender.Activitys.ShareActivity;
import com.example.yan.imagesender.R;

public class DialogAddImage extends DialogFragment implements View.OnClickListener {
    public static final int REQUEST_GALLERY = 11;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_photo, null);
        view.findViewById(R.id.btnCamera).setOnClickListener(this);
        view.findViewById(R.id.btnPhotoGallery).setOnClickListener(this);
        view.findViewById(R.id.btnCancel).setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCamera:
                ((ShareActivity)getActivity()).doCameraBtnClick();
                dismiss();
                break;
            case R.id.btnPhotoGallery:
                Intent intentGallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intentGallery.setType("image/*");
                getActivity().startActivityForResult(Intent.createChooser(intentGallery, "Select File"), REQUEST_GALLERY);
                dismiss();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
            default:
                break;
        }

    }


}
